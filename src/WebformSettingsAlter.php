<?php

namespace Drupal\webform_simplify;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Hide certain parts of Webform settings.
 */
class WebformSettingsAlter {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * WebformSettingsAlter constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
  ) {
    $this->configFactory = $configFactory;
  }

  /**
   * Hide certain parts of element forms.
   */
  public function alter(array &$form): void {
    if (webform_simplify_can_bypass()) {
      return;
    }

    $config = $this->configFactory->get('webform_simplify.settings');

    $confirmationTypes = &$form['confirmation_type']['confirmation_type']['#options'];
    $disabledConfirmationTypes = $config->get('webform_settings.disabled_confirmation_types') ?? [];
    $enabledConfirmationTypes = array_diff_key($confirmationTypes, array_flip($disabledConfirmationTypes));
    $confirmationTypes = $enabledConfirmationTypes;
  }

}
