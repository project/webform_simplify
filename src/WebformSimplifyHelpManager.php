<?php

namespace Drupal\webform_simplify;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\webform\WebformHelpManager;

/**
 * Decorated Webform help manager.
 */
class WebformSimplifyHelpManager extends WebformHelpManager {

  /**
   * {@inheritdoc}
   */
  public function buildHelp($route_name, RouteMatchInterface $route_match) {
    if ($this->isHelpDisabled()) {
      return [];
    }

    return parent::buildHelp($route_name, $route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function buildIndex() {
    if ($this->isHelpDisabled()) {
      return [];
    }

    return parent::buildIndex();
  }

  /**
   * Check if help is disabled.
   *
   * @return bool
   *   TRUE if help is disabled, FALSE otherwise.
   */
  protected function isHelpDisabled(): bool {
    return (bool) $this->configFactory
      ->get('webform_simplify.settings')
      ->get('disable_help');
  }

}
