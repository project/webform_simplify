<?php

namespace Drupal\webform_simplify\EventSubscriber;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\webform_simplify\Controller\WebformGeneralSettingsController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Hide certain parts of webform settings by restricting access to routes.
 */
class WebformRouteSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */

  protected $currentUser;

  /**
   * WebformRouteSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(
    AccountProxyInterface $currentUser,
  ) {
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::ALTER][] = ['onAlterRoutes'];

    return $events;
  }

  /**
   * Alter webform settings routes.
   */
  public function onAlterRoutes(RouteBuildEvent $event): void {
    $collection = $event->getRouteCollection();

    if ($route = $collection->get('entity.webform.settings')) {
      $defaults = $route->getDefaults();
      $defaults['_controller'] = WebformGeneralSettingsController::class;
      unset($defaults['_entity_form']);
      $route->setDefaults($defaults);

      $route->setRequirement('_permission', implode('+', [
        'edit any webform settings',
        'edit any webform general settings',
        'edit any webform form settings',
        'edit any webform submission settings',
        'edit any webform confirmation settings',
        'edit any webform asset settings',
        'edit any webform access settings',
        'edit any webform handler settings',
      ]));
    }

    if ($route = $collection->get('entity.webform.settings_form')) {
      $route->setRequirement('_permission', 'edit any webform settings+edit any webform form settings');
    }

    if ($route = $collection->get('entity.webform.settings_submissions')) {
      $route->setRequirement('_permission', 'edit any webform settings+edit any webform submission settings');
    }

    if ($route = $collection->get('entity.webform.settings_confirmation')) {
      $route->setRequirement('_permission', 'edit any webform settings+edit any webform confirmation settings');
    }

    if ($route = $collection->get('entity.webform.settings_assets')) {
      $route->setRequirement('_permission', 'edit any webform settings+edit any webform asset settings');
    }

    if ($route = $collection->get('entity.webform.settings_access')) {
      $route->setRequirement('_permission', 'edit any webform settings+edit any webform access settings');
    }

    $handlerRouteNames = [
      'entity.webform.handlers',
      'entity.webform.handler.add_email',
      'entity.webform.handler.edit_form',
      'entity.webform.handler.duplicate_form',
      'entity.webform.handler.delete_form',
      'entity.webform.handler.enable',
      'entity.webform.handler.disable',
    ];

    foreach ($handlerRouteNames as $routeName) {
      if ($route = $collection->get($routeName)) {
        $route->setRequirement('_permission', 'edit any webform settings+edit any webform handler settings');
      }
    }
  }

}
