<?php

namespace Drupal\webform_simplify\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Url;
use Drupal\webform\Element\WebformElementStates;
use Drupal\webform\WebformInterface;
use Drupal\webform_simplify\Plugin\WebformSimplifyElement\Defaults;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform Simplify settings form.
 */
class WebformSimplifySettingsForm extends FormBase {

  /**
   * The Webform Simplify Element Manager service.
   *
   * @var \Drupal\webform_simplify\WebformSimplifyElementManager
   */
  protected $webformSimplifyElementManager;
  protected ?bool $enableSuperUser = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->messenger = $container->get('messenger');
    $instance->webformSimplifyElementManager = $container->get('plugin.manager.webform_simplify.element');

    if ($container->hasParameter('security.enable_super_user')) {
      $instance->enableSuperUser = $container->getParameter('security.enable_super_user');
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'webform_simplify_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $simplifyConfig = $this->configFactory->get('webform_simplify.settings');
    $webformConfig = $this->configFactory->get('webform.settings');

    $form['#attached']['library'][] = 'webform_simplify/settings_form';

    $form['general_settings'] = [
      '#title' => $this->t('General settings'),
      '#type' => 'fieldset',
    ];

    $form['general_settings']['disable_help'] = [
      '#title' => $this->t('Disable help sections'),
      '#type' => 'checkbox',
      '#default_value' => $simplifyConfig->get('disable_help'),
    ];

    $form['general_settings']['simplify_super_user'] = [
      '#title' => $this->t('Simplify for super users'),
      '#type' => 'checkbox',
      '#default_value' => $simplifyConfig->get('simplify_super_user'),
    ];

    if ($this->enableSuperUser === FALSE) {
      $form['general_settings']['simplify_super_user']['#description'] = $this->t('Simplify the Webform UI for users with the administrator role');
    } else {
      $form['general_settings']['simplify_super_user']['#description'] = $this->t('Simplify the Webform UI for user 1 and users with the administrator role');
    }

    $form['element_types'] = [
      '#title' => $this->t('Element types'),
      '#type' => 'fieldset',
    ];

    $excludedElements = $webformConfig->get('element.excluded_elements');
    $elementSettingsUrl = Url::fromRoute('webform.config.elements', [], ['fragment' => 'edit-types'])->toString();

    $form['element_types']['info'] = [
      '#markup' => '<p>' . $this->t("Element types can be excluded so they don't appear as an option when building a webform.") . '<br>'
      . $this->t('You can do this by navigating to <i>Webforms</i> > <i>Configuration</i> > <i>Elements</i> > <i>Element types</i>, or by following <a href="@elementSettingsUrl">this link</a>.', ['@elementSettingsUrl' => $elementSettingsUrl]) . '</p>',
    ];

    $form['element_types']['excluded_elements'] = [
      '#type' => 'details',
      '#title' => $this->t('Excluded elements'),
    ];

    if ($excludedElements === []) {
      $form['element_types']['excluded_elements']['types'] = [
        '#markup' => '<p>' . $this->t('No elements are excluded yet.') . '</p>',
      ];
    }
    else {
      $form['element_types']['excluded_elements']['types'] = [
        '#markup' => '<p>' . $this->t('The following elements are excluded: <i>@excludedElements</i>.', [
          '@excludedElements' => implode(', ', $excludedElements),
        ]) . '</p>',
      ];
    }

    $form['webform_settings'] = [
      '#title' => $this->t('Webform settings'),
      '#type' => 'fieldset',
      '#tree' => TRUE,
    ];

    $confirmationTypes = [
      WebformInterface::CONFIRMATION_PAGE => $this->t('Page (redirects to new page and displays the confirmation message)'),
      WebformInterface::CONFIRMATION_INLINE => $this->t('Inline (reloads the current page and replaces the webform with the confirmation message)'),
      WebformInterface::CONFIRMATION_MESSAGE => $this->t('Message (reloads the current page/form and displays the confirmation message at the top of the page)'),
      WebformInterface::CONFIRMATION_MODAL => $this->t('Modal (reloads the current page/form and displays the confirmation message in a modal dialog)'),
      WebformInterface::CONFIRMATION_URL => $this->t('URL (redirects to a custom path or URL)'),
      WebformInterface::CONFIRMATION_URL_MESSAGE => $this->t('URL with message (redirects to a custom path or URL and displays the confirmation message at the top of the page)'),
      WebformInterface::CONFIRMATION_NONE => $this->t('None (reloads the current page and does not display a confirmation message)'),
    ];

    $disabledConfirmationTypes = $simplifyConfig->get('webform_settings.disabled_confirmation_types') ?? [];
    $enabledConfirmationTypes = array_diff(array_keys($confirmationTypes), $disabledConfirmationTypes);

    $form['webform_settings']['confirmation_types'] = [
      '#title' => $this->t('Confirmation types'),
      '#type' => 'checkboxes',
      '#options' => $confirmationTypes,
      '#default_value' => $enabledConfirmationTypes,
    ];

    $formSettings = [
      'form_settings' => $this->t('General'),
      'form_behaviors' => $this->t('Behaviors'),
      'access_denied' => $this->t('Access denied'),
      'wizard_settings' => $this->t('Wizard'),
      'preview_settings' => $this->t('Preview'),
      'file_settings' => $this->t('File'),
      'custom_settings' => $this->t('Custom'),
    ];

    $disabledFormSettings = $simplifyConfig->get('webform_settings.disabled_form_settings') ?? [];
    $enabledFormSettings = array_diff(array_keys($formSettings), $disabledFormSettings);

    $form['webform_settings']['form_settings'] = [
      '#title' => $this->t('Form settings'),
      '#type' => 'checkboxes',
      '#options' => $formSettings,
      '#default_value' => $enabledFormSettings,
    ];

    $submissionsSettings = [
      'submission_settings' => $this->t('General'),
      'submission_behaviors' => $this->t('Behaviors'),
      'submission_user_settings' => $this->t('User'),
      'submission_access' => $this->t('Access token'),
      'access_denied' => $this->t('Access denied'),
      'submission_limits' => $this->t('Limit'),
      'purge_settings' => $this->t('Purge'),
      'draft_settings' => $this->t('Draft'),
      'autofill_settings' => $this->t('Autofill'),
      'views_settings' => $this->t('Views'),
    ];

    $disabledSubmissionsSettings = $simplifyConfig->get('webform_settings.disabled_submissions_settings') ?? [];
    $enabledSubmissionsSettings = array_diff(array_keys($submissionsSettings), $disabledSubmissionsSettings);

    $form['webform_settings']['submissions_settings'] = [
      '#title' => $this->t('Submissions settings'),
      '#type' => 'checkboxes',
      '#options' => $submissionsSettings,
      '#default_value' => $enabledSubmissionsSettings,
    ];

    $form['element_settings'] = [
      '#title' => $this->t('Element settings'),
      '#type' => 'fieldset',
      '#tree' => TRUE,
    ];

    $definitions = $this->webformSimplifyElementManager->getDefinitions();

    // Sort the definitions by label.
    uasort($definitions, function ($a, $b) {
      return strcasecmp($a['label'], $b['label']);
    });

    // Move the defaults to the top.
    $definitions = [Defaults::ID => $definitions[Defaults::ID]] + $definitions;

    foreach ($definitions as $id => $definition) {
      if (in_array($id, $excludedElements)) {
        continue;
      }

      /** @var \Drupal\webform_simplify\WebformSimplifyElementInterface $simplifyElement */
      $simplifyElement = $this->webformSimplifyElementManager->createInstance($id);

      $form['element_settings'][$id] = [
        '#title' => $definition['label'],
        '#type' => 'details',
        '#tree' => TRUE,
        '#open' => FALSE,
      ];

      $tabs = $simplifyElement->getTabs();
      $disabledTabs = $this->getElementSetting($id, 'disabled_tabs') ?? [];
      $enabledTabs = array_diff(array_keys($tabs), $disabledTabs);

      $form['element_settings'][$id]['tabs'] = [
        '#title' => $this->t('Tabs'),
        '#type' => 'checkboxes',
        '#options' => $tabs,
        '#default_value' => $enabledTabs,
      ];

      $features = $simplifyElement->getFeatures();
      $disabledFeatures = $this->getElementSetting($id, 'disabled_features') ?? [];
      $enabledFeatures = array_diff(array_keys($features), $disabledFeatures);

      $form['element_settings'][$id]['features'] = [
        '#title' => $this->t('Features'),
        '#type' => 'checkboxes',
        '#options' => $features,
        '#default_value' => $enabledFeatures,
      ];

      $form['element_settings'][$id]['conditions'] = [
        '#title' => $this->t('Conditions'),
        '#type' => 'fieldset',
      ];

      $states = OptGroup::flattenOptions(WebformElementStates::getStateOptions());
      $disabledStates = $this->getElementSetting($id, 'disabled_condition_states') ?? [];
      $enabledStates = array_diff(array_keys($states), $disabledStates);

      $form['element_settings'][$id]['conditions']['states'] = [
        '#title' => $this->t('States'),
        '#type' => 'checkboxes',
        '#multiple' => TRUE,
        '#options' => $states,
        '#default_value' => $enabledStates,
      ];

      $operators = [
        'and' => t('All'),
        'or' => t('Any'),
        'xor' => t('One'),
      ];
      $disabledOperators = $this->getElementSetting($id, 'disabled_condition_operators') ?? [];
      $enabledOperators = array_diff(array_keys($operators), $disabledOperators);

      $form['element_settings'][$id]['conditions']['operators'] = [
        '#title' => $this->t('Operators'),
        '#type' => 'checkboxes',
        '#options' => $operators,
        '#default_value' => $enabledOperators,
      ];

      $triggers = WebformElementStates::getTriggerOptions();
      $disabledTriggers = $this->getElementSetting($id, 'disabled_condition_triggers') ?? [];
      $enabledTriggers = array_diff(array_keys($triggers), $disabledTriggers);

      $form['element_settings'][$id]['conditions']['triggers'] = [
        '#title' => $this->t('Triggers'),
        '#type' => 'checkboxes',
        '#options' => $triggers,
        '#default_value' => $enabledTriggers,
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory->getEditable('webform_simplify.settings');
    $values = $form_state->cleanValues()->getValues();

    $config->set('disable_help', $values['disable_help']);
    $config->set('simplify_super_user', $values['simplify_super_user']);

    $disabledConfirmationTypes = array_filter(
      $values['webform_settings']['confirmation_types'],
      function ($value) {
        return $value === 0;
      }
    );
    $config->set('webform_settings.disabled_confirmation_types', array_keys($disabledConfirmationTypes));

    $disabledFormSettings = array_filter(
      $values['webform_settings']['form_settings'],
      function ($value) {
        return $value === 0;
      }
    );
    $config->set('webform_settings.disabled_form_settings', array_keys($disabledFormSettings));

    $disabledSubmissionsSettings = array_filter(
      $values['webform_settings']['submissions_settings'],
      function ($value) {
        return $value === 0;
      }
    );
    $config->set('webform_settings.disabled_submissions_settings', array_keys($disabledSubmissionsSettings));

    foreach ($values['element_settings'] as $element => $settings) {
      $disabledTabs = array_filter(
        $settings['tabs'],
        function ($value) {
          return $value === 0;
        }
      );

      $config->set(
        sprintf('element_settings.%s.disabled_tabs', $element),
        array_keys($disabledTabs)
      );

      $disabledFeatures = array_filter(
        $settings['features'],
        function ($value) {
          return $value === 0;
        }
      );

      $config->set(
        sprintf('element_settings.%s.disabled_features', $element),
        array_keys($disabledFeatures)
      );

      $disabledStates = array_filter(
        $settings['conditions']['states'],
        function ($value) {
          return $value === 0;
        }
      );

      $config->set(
        sprintf('element_settings.%s.disabled_condition_states', $element),
        array_keys($disabledStates)
      );

      $disabledTriggers = array_filter(
        $settings['conditions']['triggers'],
        function ($value) {
          return $value === 0;
        }
      );

      $config->set(
        sprintf('element_settings.%s.disabled_condition_triggers', $element),
        array_keys($disabledTriggers)
      );

      $disabledOperators = array_filter(
        $settings['conditions']['operators'],
        function ($value) {
          return $value === 0;
        }
      );

      $config->set(
        sprintf('element_settings.%s.disabled_condition_operators', $element),
        array_keys($disabledOperators)
      );
    }

    // Clean up config.
    $elementSettings = $config->get('element_settings');
    $defaultSettings = $elementSettings[Defaults::ID] ?? [];

    foreach ($elementSettings as $element => &$settings) {
      if (($settings['disabled_tabs'] ?? []) === [] && ($settings['disabled_features'] ?? []) === []) {
        unset($elementSettings[$element]);
      }

      // If the settings are the same as the defaults, remove them
      // (except for the defaults themselves).
      if ($element !== Defaults::ID) {
        foreach (array_keys($settings) as $settingKey) {
          if (array_diff($defaultSettings[$settingKey], $settings[$settingKey]) !== []) {
            continue;
          }
          if (array_diff($settings[$settingKey], $defaultSettings[$settingKey]) !== []) {
            continue;
          }
          unset($settings[$settingKey]);
        }
        if ($settings === []) {
          unset($elementSettings[$element]);
        }
      }
    }

    $config->set('element_settings', $elementSettings);
    $config->save();
    $this->messenger->addStatus($this->t('The configuration options have been saved.'));
  }

  /**
   * Get an element setting, with a fallback to the default settings.
   *
   * @param string $elementKey
   *   The element key.
   * @param string $settingKey
   *   The setting key.
   *
   * @return mixed
   *   The setting value.
   */
  protected function getElementSetting(string $elementKey, string $settingKey) {
    $simplifyConfig = $this->configFactory->get('webform_simplify.settings');

    return $simplifyConfig->get(sprintf('element_settings.%s.%s', $elementKey, $settingKey))
      ?? $simplifyConfig->get(sprintf('element_settings.%s.%s', Defaults::ID, $settingKey));
  }

}
