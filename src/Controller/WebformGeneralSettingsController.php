<?php

namespace Drupal\webform_simplify\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\webform\WebformInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller for webform general settings.
 */
class WebformGeneralSettingsController implements ContainerInjectionInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->currentUser = $container->get('current_user');

    return $instance;
  }

  /**
   * Displays the webform general settings form.
   *
   * If the general settings form is inaccessible, any other accessible
   * settings form is displayed.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect to one of the settings forms.
   */
  public function __invoke(WebformInterface $webform): RedirectResponse {
    $linkTemplates = [
      'settings_general',
      'settings_form',
      'settings_submissions',
      'settings_confirmation',
      'settings_assets',
      'settings_access',
    ];

    foreach ($linkTemplates as $linkTemplate) {
      $url = Url::fromRoute("entity.webform.$linkTemplate", ['webform' => $webform->id()]);
      if ($url->access()) {
        return new RedirectResponse($url->toString());
      }
    }

    throw new AccessDeniedHttpException();
  }

}
