<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Email element.
 *
 * @WebformSimplifyElement(
 *     id = "email",
 *     label = @Translation("Email"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\Email
 */
class Email extends TextBase {
}
