<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Number element for Webform Simplify.
 *
 * @WebformSimplifyElement(
 *   id = "number",
 *   label = @Translation("Number"),
 *   provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\Number
 */
class Number extends NumericBase {
}
