<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Flexbox Layout element.
 *
 * @WebformSimplifyElement(
 *     id = "webform_flexbox",
 *     label = @Translation("Flexbox layout"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformFlexbox
 */
class WebformFlexbox extends ContainerBase {

  /**
   * {@inheritdoc}
   */
  public function getFeatures(): array {
    return [
      'align_items' => $this->t('Align items'),
    ] + parent::getFeatures();
  }

  /**
   * {@inheritdoc}
   */
  public function getFeaturePropertyMap(): array {
    return [
      'align_items' => [
        'properties.flexbox.align_items',
      ],
    ] + parent::getFeaturePropertyMap();
  }

}
