<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Autocomplete element.
 *
 * @WebformSimplifyElement(
 *     id = "webform_autocomplete",
 *     label = @Translation("Autocomplete"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformAutocomplete
 *
 * @todo Add element-specific properties
 */
class WebformAutocomplete extends TextBase {

  /**
   * {@inheritdoc}
   */
  public function getFeatures(): array {
    $features = parent::getFeatures();
    unset($features['autocomplete']);

    return $features;
  }

  /**
   * {@inheritdoc}
   */
  public function getFeaturePropertyMap(): array {
    $map = parent::getFeaturePropertyMap();
    unset($map['autocomplete']);

    return $map;
  }

}
