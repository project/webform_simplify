<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Default Values element for Webform Simplify.
 *
 * @WebformSimplifyElement(
 *     id = \Drupal\webform_simplify\Plugin\WebformSimplifyElement\Defaults::ID,
 *     label = @Translation("Default values"),
 *     provider = "webform_simplify",
 * )
 */
class Defaults extends WebformSimplifyElementBase {

  const ID = '_defaults';

}
