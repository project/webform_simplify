<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Managed File element.
 *
 * @WebformSimplifyElement(
 *     id = "managed_file",
 *     label = @Translation("File"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\ManagedFile
 */
class ManagedFile extends WebformManagedFileBase {
}
