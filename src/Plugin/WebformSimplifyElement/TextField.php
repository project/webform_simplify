<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Text Field element.
 *
 * @WebformSimplifyElement(
 *     id = "textfield",
 *     label = @Translation("Text field"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\TextField
 */
class TextField extends TextBase {
}
