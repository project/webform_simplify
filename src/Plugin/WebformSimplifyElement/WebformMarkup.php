<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Markup element.
 *
 * @WebformSimplifyElement(
 *     id = "webform_markup",
 *     label = @Translation("Basic HTML"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformMarkup
 */
class WebformMarkup extends WebformMarkupBase {

  /**
   * {@inheritdoc}
   */
  public function getFeatures(): array {
    return [
      'markup' => $this->t('HTML markup'),
    ] + parent::getFeatures();
  }

  /**
   * {@inheritdoc}
   */
  public function getFeaturePropertyMap(): array {
    return [
      'markup' => [
        'properties.markup.markup',
      ],
    ] + parent::getFeaturePropertyMap();
  }

}
