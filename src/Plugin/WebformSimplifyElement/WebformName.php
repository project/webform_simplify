<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Name element.
 *
 * @WebformSimplifyElement(
 *   id = "webform_name",
 *   label = @Translation("Name"),
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformName
 */
class WebformName extends WebformCompositeBase {
}
