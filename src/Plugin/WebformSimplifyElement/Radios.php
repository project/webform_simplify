<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Radios element.
 *
 * @WebformSimplifyElement(
 *     id = "radios",
 *     label = @Translation("Radios"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\Radios
 */
class Radios extends OptionsBase {
}
