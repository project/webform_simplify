<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Audio File element.
 *
 * @WebformSimplifyElement(
 *     id = "webform_audio_file",
 *     label = @Translation("Audio file"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformAudioFile
 */
class WebformAudioFile extends WebformManagedFileBase {
}
