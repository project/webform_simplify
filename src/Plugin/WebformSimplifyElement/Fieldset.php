<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Fieldset element.
 *
 * @WebformSimplifyElement(
 *     id = "fieldset",
 *     label = @Translation("Fieldset"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\Fieldset
 */
class Fieldset extends ContainerBase {
}
