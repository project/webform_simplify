<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Document File element.
 *
 * @WebformSimplifyElement(
 *     id = "webform_document_file",
 *     label = @Translation("Document file"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformDocumentFile
 */
class WebformDocumentFile extends WebformManagedFileBase {
}
