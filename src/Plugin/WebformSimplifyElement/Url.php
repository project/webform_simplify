<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform URL element.
 *
 * @WebformSimplifyElement(
 *     id = "url",
 *     label = @Translation("URL"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\Url
 */
class Url extends TextBase {
}
