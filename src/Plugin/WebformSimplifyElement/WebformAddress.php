<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Address element.
 *
 * @WebformSimplifyElement(
 *   id = "webform_address",
 *   label = @Translation("Address"),
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformAddress
 */
class WebformAddress extends WebformCompositeBase {
}
