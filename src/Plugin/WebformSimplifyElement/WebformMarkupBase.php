<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Provides base functionality for markup elements in webforms.
 *
 * @see \Drupal\webform\Plugin\WebformElement\WebformMarkupBase
 */
abstract class WebformMarkupBase extends WebformSimplifyElementBase {

  /**
   * {@inheritdoc}
   */
  public function getFeatures(): array {
    $features = parent::getFeatures();
    unset($features['prepopulate']);

    return [
      'display_on' => $this->t('Display on'),
    ] + $features;
  }

  /**
   * {@inheritdoc}
   */
  public function getFeaturePropertyMap(): array {
    return [
      'display_on' => [
        'properties.markup.display_on',
      ],
    ] + parent::getFeaturePropertyMap();
  }

}
