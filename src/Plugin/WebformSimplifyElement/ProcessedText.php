<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Processed Text element.
 *
 * @WebformSimplifyElement(
 *     id = "processed_text",
 *     label = @Translation("Advanced HTML/Text"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformMarkup
 */
class ProcessedText extends WebformMarkupBase {

  /**
   * {@inheritdoc}
   */
  public function getFeatures(): array {
    return [
      'text' => $this->t('Text'),
    ] + parent::getFeatures();
  }

  /**
   * {@inheritdoc}
   */
  public function getFeaturePropertyMap(): array {
    return [
      'text' => [
        'properties.markup.text',
      ],
    ] + parent::getFeaturePropertyMap();
  }

}
