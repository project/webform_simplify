<?php

namespace Drupal\webform_simplify\Plugin\WebformSimplifyElement;

/**
 * Defines the Webform Video File element.
 *
 * @WebformSimplifyElement(
 *     id = "webform_video_file",
 *     label = @Translation("Video file"),
 *     provider = "webform",
 * )
 * @see \Drupal\webform\Plugin\WebformElement\WebformVideoFile
 */
class WebformVideoFile extends WebformManagedFileBase {
}
