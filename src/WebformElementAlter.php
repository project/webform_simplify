<?php

namespace Drupal\webform_simplify;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\webform\Element\WebformElementStates;
use Drupal\webform_simplify\Plugin\WebformSimplifyElement\Defaults;

/**
 * Hide certain parts of element forms.
 */
class WebformElementAlter {

  use DependencySerializationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */

  protected $configFactory;

  /**
   * The element manager for Webform Simplify module.
   *
   * @var \Drupal\webform_simplify\WebformSimplifyElementManager
   */
  protected $webformSimplifyElementManager;

  /**
   * WebformElementAlter constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\webform_simplify\WebformSimplifyElementManager $webformSimplifyElementManager
   *   The element manager for Webform Simplify module.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    WebformSimplifyElementManager $webformSimplifyElementManager,
  ) {
    $this->configFactory = $configFactory;
    $this->webformSimplifyElementManager = $webformSimplifyElementManager;
  }

  /**
   * Hide certain parts of element forms.
   */
  public function alter(array &$form): void {
    if (webform_simplify_can_bypass()) {
      return;
    }

    $element = $form['properties']['type']['#value'];
    if (!$this->webformSimplifyElementManager->hasDefinition($element)) {
      return;
    }

    $simplifyElement = $this->webformSimplifyElementManager->createInstance($element);
    assert($simplifyElement instanceof WebformSimplifyElementInterface);

    // Hide certain tabs.
    $disabledTabs = $this->getElementSetting($element, 'disabled_tabs') ?? [];
    foreach ($disabledTabs as $tab) {
      $this->hideTab($form, $tab);
    }

    // Hide certain features.
    $disabledFeatures = $this->getElementSetting($element, 'disabled_features') ?? [];
    foreach ($disabledFeatures as $feature) {
      $this->hideFeature($form, $simplifyElement, $feature);
    }

    if (isset($form['properties']['conditional_logic']['states'])) {
      // Hide conditional logic states.
      $disabledConditionStates = $this->getElementSetting($element, 'disabled_condition_states') ?? [];
      $conditionStatesByGroup = &$form['properties']['conditional_logic']['states']['#state_options'];
      foreach ($conditionStatesByGroup as $group => &$states) {
        $states = array_diff_key($states, array_flip($disabledConditionStates));
        if (empty($states)) {
          unset($conditionStatesByGroup[$group]);
        }
      }

      // Hide conditional logic triggers.
      $disabledConditionTriggers = $this->getElementSetting($element, 'disabled_condition_triggers') ?? [];
      $conditionTriggers = &$form['properties']['conditional_logic']['states']['#trigger_options'];
      $conditionTriggers ??= WebformElementStates::getTriggerOptions();
      $conditionTriggers = array_diff_key($conditionTriggers, array_flip($disabledConditionTriggers));

      // Hide conditional logic operators.
      $form['properties']['conditional_logic']['states']['#after_build'][] = [$this, 'afterBuild'];
    }
  }

  /**
   * After build callback to hide certain condition operators.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   The form element.
   *
   * @see \Drupal\webform_simplify\WebformElementAlter::alter()
   */
  public function afterBuild(array $element, FormStateInterface $formState): array {
    $form = $formState->getCompleteForm();
    $elementName = $form['properties']['type']['#value'];
    $disabledConditionOperators = $this->getElementSetting($elementName, 'disabled_condition_operators') ?? [];

    $stateRow = &$element['states'][0];
    $stateRow['operator']['#options'] = array_diff_key($stateRow['operator']['#options'], array_flip($disabledConditionOperators));

    return $element;
  }

  /**
   * Hide a certain tab from an element form.
   */
  protected function hideTab(array &$form, string $fragment): void {
    if (!isset($form['properties']['tabs']['items']['#items'])) {
      return;
    }

    foreach ($form['properties']['tabs']['items']['#items'] as $key => $tab) {
      /** @var \Drupal\Core\Url $url */
      $url = $tab['#url'];
      if ($url->getOption('fragment') === $fragment) {
        unset($form['properties']['tabs']['items']['#items'][$key]);
      }
    }
  }

  /**
   * Hide a certain feature from an element form.
   */
  protected function hideFeature(array &$form, WebformSimplifyElementInterface $simplifyElement, string $feature): void {
    $map = $simplifyElement->getFeaturePropertyMap();

    foreach ($map[$feature] as $property) {
      $parents = explode('.', $property);

      if (!NestedArray::keyExists($form, $parents)) {
        continue;
      }

      // Hide element.
      $element = &NestedArray::getValue($form, $parents);
      $element['#access'] = FALSE;

      // Hide parent if empty.
      array_pop($parents);
      $elementParent = &NestedArray::getValue($form, $parents);
      $this->hideIfChildrenAreEmpty($elementParent);
    }
  }

  /**
   * Hide a container if all its children are hidden.
   */
  protected function hideIfChildrenAreEmpty(array &$form): void {
    $children = Element::children($form);
    $empty = TRUE;

    foreach ($children as $child) {
      if (!isset($form[$child]['#access']) || $form[$child]['#access'] === TRUE) {
        $empty = FALSE;
      }
    }

    if ($empty) {
      $form['#access'] = FALSE;
    }
  }

  /**
   * Get an element setting, with a fallback to the default settings.
   *
   * @param string $elementKey
   *   The element key.
   * @param string $settingKey
   *   The setting key.
   *
   * @return mixed
   *   The setting value.
   */
  protected function getElementSetting(string $elementKey, string $settingKey) {
    $simplifyConfig = $this->configFactory->get('webform_simplify.settings');

    return $simplifyConfig->get(sprintf('element_settings.%s.%s', $elementKey, $settingKey))
      ?? $simplifyConfig->get(sprintf('element_settings.%s.%s', Defaults::ID, $settingKey));
  }

}
