<?php

namespace Drupal\webform_simplify;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Hide certain parts of Webform form settings.
 */
class WebformFormSettingsAlter {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * WebformSettingsAlter constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory
  ) {
    $this->configFactory = $configFactory;
  }

  /**
   * Hide certain parts of element forms.
   */
  public function alter(array &$form): void {
    if (webform_simplify_can_bypass()) {
      return;
    }

    $config = $this->configFactory->get('webform_simplify.settings');

    $disabledSubmissionsSettings = $config->get('webform_settings.disabled_form_settings') ?? [];
    foreach ($disabledSubmissionsSettings as $disabledSetting) {
      $form[$disabledSetting]['#access'] = FAlSE;
    }
  }

}
