<?php

namespace Drupal\webform_simplify;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Service modifier for Webform Simplify.
 */
class WebformSimplifyServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->has('webform.help_manager')) {
      $definition = $container->getDefinition('webform.help_manager');
      $definition->setClass(WebformSimplifyHelpManager::class);
    }
  }

}
