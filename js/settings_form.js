(function () {

  'use strict';
  Drupal.behaviors.webformSimplifyDefaultOptions = {
    attach: function (context, settings) {
      once('webform-simplify-settings-form', '#edit-element-settings-defaults input[type="checkbox"]', context).forEach(function (element) {
        element.addEventListener('change', function (event) {
          const checkboxNameParts = event.target.getAttribute('name').split('_defaults');
          const otherCheckboxes = document.querySelectorAll(`[name^="${checkboxNameParts[0]}"][name$="${checkboxNameParts[1]}"]`)

          otherCheckboxes.forEach(function (checkbox) {
            checkbox.checked = event.target.checked;
          });
        });
      });
    }
  };

})();
